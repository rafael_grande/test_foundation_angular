angular.
  module('orcamentoApp').
  config(['$locationProvider', '$routeProvider',
    function config($locationProvider, $routeProvider) {
      $locationProvider.hashPrefix('!');

      $routeProvider.
        when('/', {
          template: '<slider></slider><menu-category></menu-category><product-list></product-list>'
        }).
        when('/product/:productId', {
          template: '<menu-category></menu-category><product-detail></product-detail>'
        }).
        when('/cart', {
          template: '<list-cart></list-cart>'
        }).
        otherwise('/');
    }
  ]);