angular.
  module('productList').
  component('productList', {
    templateUrl: 'js/app/product-list/product-list.template.html',
    controller: ['$http',
      function ProductListController($http) {
        var self = this;
        var products;
          
        $http.get('js/app/data/products.json').then(function(response) {
           self.products = response.data; 
        });


        self.addCart = function(elem){
          console.log(elem);
          elem["qtd"]=10;  
          
          sessionStorage.getItem('produtos') != null? products = JSON.parse(sessionStorage.getItem('produtos')) : products = {produts:[]};
          products.produts[products.produts.length] = {
                                                        id: elem.id, 
                                                        name: elem.name,
                                                        qtd: elem.qtd
                                                      };

          var prod_orc = JSON.stringify(products);
          sessionStorage.setItem('produtos', prod_orc);
          var qtde_cart = document.getElementById("cart");
          qtde_cart.innerText= Number(qtde_cart.innerText)+1;
        }
      }
    ]
  });