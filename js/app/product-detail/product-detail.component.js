angular.
  module('productDetail').
  component('productDetail', {
    //template: '<span>{{$ctrl.productId}}</span>',
    templateUrl: 'js/app/product-detail/product-detail.template.html',
    controller: ['$http', '$routeParams',
      function ProductDetailController($http, $routeParams) {
        var self = this;

        $http.get('js/app/data/product/product-' + $routeParams.productId + '.json').then(function(response) {
          self.product = response.data;
        });
      }
    ]
  });